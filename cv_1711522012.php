<!DOCTYPE html>
<html>
<head>
	<title>Biodata Diri</title>
</head>
<body style="background-color: grey">


	<h1  align="center" style="color: white";><b><u>Curriculum Vitae</u></b></h1>
	<br>
	<p  align="center"><img src="afif.jpg" style="height: 200px;width: 200px"></p>
	<br>

<div style="background-color: white;width:35%;height: 100% ">	
	<h2 align="left"><u>Data Diri</u></h1>
	<table>
		<tr>
			<td>Nama</td><td>:</td>
			<td>Afif Maulana Isman</td>
		</tr>
		<tr>
			<td>Tempat/Tanggal lahir</td><td>:</td>
			<td>Padang/30 November 1998</td>
		</tr>
		<tr>
			<td>NIM</td><td>:</td>
			<td>1711522012</td>
		</tr>
		<tr>
			<td>Fakultas</td><td>:</td>
			<td>Teknologi Informasi</td>
		</tr>
		<tr>
			<td>Jurusan</td><td>:</td>
			<td>Sistem Informasi</td>
		</tr>
		<tr>
			<td>Jenis Kelamin</td><td>:</td>
			<td>Laki-laki</td>
		</tr>
		<tr>
			<td>Alamat</td><td>:</td>
			<td>Jln.Sutomo nomor 59F</td>
		</tr>
		<tr>
			<td>Telepon</td><td>:</td>
			<td>082169075855</td>
		</tr>
		<tr>
			<td>e-Mail</td><td>:</td>
			<td>afif.maulana.isman</td>
		</tr>

	</table>
</div>

<div style="background-color: white;width:35%;height: 100% ">
	<h2 align="Left"><u>Pendidikan Formal</u></h2>
	<table>
		<tr>
			<td>TK Pertiwi</td>
		</tr>
		<tr>
			<td>SD Kartika 1-12</td>
		</tr>
		<tr>
			<td>SMP Negeri 8 Padang</td>
		</tr>
		<tr>
			<td>SMA Negeri 10 Padang</td>
		</tr>
	</table>
</div>

<div style="background-color: white;width:35%;height: 100% ">
	<h2 align="left"><u>Organisasi yang Pernah Diikuti</u></h2>
	<table>
		<tr>
			<td>SMA</td><td>:</td>
		</tr>
		<tr>
			<td></td><td></td><td>English Club</td>
		</tr>
		<tr>
			<td></td><td></td><td>Olimpiade Komputer</td>
		</tr>
		<tr>
			<td></td><td></td><td>Persatuan Bulutangkis</td>
		</tr>

		<tr>
			<td>SMP</td><td>:</td>
		</tr>
		<tr>
			<td></td><td></td><td>Marching Band</td>
		</tr>
		<tr>
			<td></td><td></td><td>Vocal Grup</td>
		</tr>

		<tr>
			<td>SD</td><td>:</td>
		</tr>
		<tr>
			<td></td><td></td><td>Marching Band</td>
		</tr>
	</table>
</div>

<div style="background-color: white;width:35%;height: 100% ">
	<h2 align="left"><u>Kemampuan</u></h2>
	<table>
		<tr>
			<td>Bahasa Inggris</td>
		</tr>
		<tr>
			<td>Menyanyi</td>
		</tr>
		<tr>
			<td>C++</td>
		</tr>
	</table>
</div>

</body>
</html>